// jshint devel:true
window.onload = () => {
  $("[data-id='header-site']").load('../templates/header-site.html');
  $("[data-id='main-content']").load('../templates/main-content.html', () => {
    site.heroFade();
  });
  $("[data-id='footer-site']").load('../templates/footer-site.html', () => {
    if (site.isMobile()) {
      site.accordion();
    };
  });
}

let site = {
  accordion: () => {
    // the ul element has to direct sibiling of the toggle element for this to work
    $("[data-id='accordion-toggle'] ~ ul, [data-id='accordion-toggle-hide']").hide();
    $("[data-id='accordion-toggle']").on("click", function() {
      $(this).next().toggle('fast');
      $(this).find("[data-id='accordion-toggle-show'], [data-id='accordion-toggle-hide']").toggle();
    });
  },

  isMobile: () => {
    if ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      return true;
    }
    else {
      return false;
    }
  },

  heroFade: () => {
    var heroItems = $(".hero").children();
    var heroItemsIndex = 0;
    function fade() {
      $(heroItems[heroItemsIndex]).fadeOut();
      heroItemsIndex = (heroItemsIndex + 1) % heroItems.length;
      $(heroItems[heroItemsIndex]).fadeIn();
      setTimeout(fade, 5000);
    }
    fade();
  }
}
